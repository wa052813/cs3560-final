// CS3560 Final Part B code
// William Adams wa052813@ohio.edu
#include<iostream>
#include<cstdlib>

#include<string>
using namespace std;

int countLine(string a); //counts number of input lines
int countChar(string b); //counts number of input characters

int main(){
 string a; //initializes a string
 cout<<"Please input your string\n";
 cin>> a;
 cout<< countLine(a) <<endl; //evaluates that string and prints result
 cout<< countChar(a) <<endl; //evaluates the string and prints result
 return 0;
 char* input;
}

int countLine(string a){ //counts number of input lines
 int lines= 0;
 for(int i=0; i< a.size(); i++){
  if(a[i] == '\n'){
  lines++;
  }
 }
 return lines;
}

int countChar(string b){
 int characters=0;
 for(int i=0; i< b.size(); i++){
  characters++;
 }
 return characters;
}
